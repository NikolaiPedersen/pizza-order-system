/**
 * @Author: Nikolai Pedersen
 * @Version: 1.0
 * @Tester: Hatem & Maamoun (version 0.9)
 * @Description: A pizza ordering system with the abilities to add extra items and choose size.
 * Can add infinite pizzas but is not capable of deleting previous entries.
 */
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Welcome to Java Pizza Order System. " +
                "\nYou will navigate by entering 0 to 10 or typing yes/no. \"Exit\" will cancel your order.\n");

        int finalPrice = 0;
        boolean orderComplete = false;
        while (!orderComplete) {

            double collectedPrice = selectPizza() + selectTopping();
            collectedPrice = selectSize(collectedPrice);

            // determine if program will loop or end
            boolean isValid = false;
            do {
                System.out.print("Would you like to add another pizza? Enter yes/no: ");
                String anotherOne = in.next();
                if (!in.hasNextLine()) {
                    in.next();
                } else if (anotherOne.equalsIgnoreCase("yes")) {
                    finalPrice += collectedPrice;
                    isValid = true;
                } else if (anotherOne.equalsIgnoreCase("no")) {
                    finalPrice += collectedPrice;
                    System.out.println("\nThe total price of your order is DKK " + finalPrice);
                    isValid = true;
                    orderComplete = true;
                }
            } while (!isValid);
        }
    }

    public static int selectPizza() {
        Scanner in = new Scanner(System.in);

        int priceOfPizza = 0;
        final int PIZZA1_PRICE = 60;
        final int PIZZA2_PRICE = 70;
        final int PIZZA3_PRICE = 70;
        final int PIZZA4_PRICE = 80;
        final int PIZZA5_PRICE = 75;
        final int PIZZA6_PRICE = 40;
        final int PIZZA7_PRICE = 60;
        final int PIZZA8_PRICE = 70;
        final int PIZZA9_PRICE = 80;
        final int PIZZA10_PRICE = 45;

        boolean pizzaChosen = false;
        while (!pizzaChosen) {
            System.out.print("Enter menucard? Enter yes/no/exit: ");
            String userAnswer = in.next();

            if (userAnswer.equalsIgnoreCase("no")) {
                continue;
            }
            else if (userAnswer.equalsIgnoreCase("exit")) {
                System.out.println("Canceling ... ");
                System.exit(0);
            }
            else if (userAnswer.equalsIgnoreCase("yes")) {
                menucard();
                boolean isValidInput = false;
                while (!isValidInput) {
                    int choice;
                    while (true) {
                        try {
                            System.out.print("Type number to enter decision: ");
                            choice = in.nextInt();
                            break;
                        }
                        catch (Exception e) {
                        in.next();
                            }
                    }
                        if (choice >= 0 && choice <= 10) {
                            switch (choice) {
                                case 0:
                                    break;
                                case 1:
                                    priceOfPizza += PIZZA1_PRICE;
                                    break;
                                case 2:
                                    priceOfPizza += PIZZA2_PRICE;
                                    break;
                                case 3:
                                    priceOfPizza += PIZZA3_PRICE;
                                    break;
                                case 4:
                                    priceOfPizza += PIZZA4_PRICE;
                                    break;
                                case 5:
                                    priceOfPizza += PIZZA5_PRICE;
                                    break;
                                case 6:
                                    priceOfPizza += PIZZA6_PRICE;
                                    break;
                                case 7:
                                    priceOfPizza += PIZZA7_PRICE;
                                    break;
                                case 8:
                                    priceOfPizza += PIZZA8_PRICE;
                                    break;
                                case 9:
                                    priceOfPizza += PIZZA9_PRICE;
                                    break;
                                case 10:
                                    priceOfPizza += PIZZA10_PRICE;
                                    break;
                            }
                            pizzaChosen = choice != 0;
                            isValidInput = true;
                        }
                }
            }
        }
        return priceOfPizza;
    }
    public static int selectTopping() {
        Scanner in = new Scanner(System.in);

        int priceOfTopping = 0;
        final int PRICE_CHEESE = 10;
        final int PRICE_TRUFFLE = 20;
        final int PRICE_GARLIC = 5;
        final int PRICE_MUSHROOMS = 5;
        final int PRICE_PEPPERS = 5;
        final int PRICE_ONION = 5;
        final int PRICE_HAM = 8;
        final int PRICE_PEPPERONI = 8;
        final int PRICE_KEBAB = 8;
        final int PRICE_SALAD = 3;

        // program will loop as long as the user wants to add additional toppings
        boolean toppingChosen = false;
        while (!toppingChosen) {
            System.out.print("Would you like to add additional topping? Enter yes/no: ");
            String userAnswer = in.next();

            if (userAnswer.equalsIgnoreCase("no")) {
                return priceOfTopping;
            }
            else if (userAnswer.equalsIgnoreCase("yes")) {
                System.out.println("Select your topping: ");
                toppings();

                System.out.print("Type number to enter decision: ");
                int choice = in.nextInt();
                switch (choice) {
                    case 0:
                        break;
                    case 1:
                        priceOfTopping += PRICE_CHEESE;
                        break;
                    case 2:
                        priceOfTopping += PRICE_TRUFFLE;
                        break;
                    case 3:
                        priceOfTopping += PRICE_GARLIC;
                        break;
                    case 4:
                        priceOfTopping += PRICE_MUSHROOMS;
                        break;
                    case 5:
                        priceOfTopping += PRICE_PEPPERS;
                        break;
                    case 6:
                        priceOfTopping += PRICE_ONION;
                        break;
                    case 7:
                        priceOfTopping += PRICE_HAM;
                        break;
                    case 8:
                        priceOfTopping += PRICE_PEPPERONI;
                        break;
                    case 9:
                        priceOfTopping += PRICE_KEBAB;
                        break;
                    case 10:
                        priceOfTopping += PRICE_SALAD;
                        break;
                }
            }
        }
        return priceOfTopping;
    }
    public static double selectSize(double collectedPrice) {
        Scanner in = new Scanner(System.in);

        // calculate price of pizza * size
        final double SIZE_CHILD = 0.75;
        final double SIZE_STANDARD = 1;
        final double SIZE_FAMILY = 1.5;
        double childSizePrice = collectedPrice * SIZE_CHILD;
        double standardSizePrice = collectedPrice * SIZE_STANDARD;
        double familySizePrice = collectedPrice * SIZE_FAMILY;

        boolean sizeChosen = false;
        while (!sizeChosen) {
            System.out.print("Would you like to choose a size? Enter yes/no: ");
            String userAnswer = in.next();

            if (userAnswer.equalsIgnoreCase("no")) {
                break;
            }
            else if (userAnswer.equalsIgnoreCase("yes")) {
                System.out.println("Select your size: ");
                sizes(childSizePrice, standardSizePrice, familySizePrice);

                System.out.print("Type number to enter decision: ");
                int choice = in.nextInt();
                switch (choice) {
                    case 1:
                        collectedPrice = childSizePrice;
                        break;
                    case 2:
                        collectedPrice = standardSizePrice;
                        break;
                    case 3:
                        collectedPrice = familySizePrice;
                        break;
                }
            }
            sizeChosen = true;
        }
        return collectedPrice;
    }
    private static void menucard() {
        System.out.println("0.\tGo back");
        System.out.printf("%-20.20s %-30.30s %-20.20s%n", "1.\tMargherita:", "tomato, cheese, basil", "Price DKK 60");
        System.out.printf("%-20.20s %-30.30s %-20.20s%n", "2.\tProsciutto:", "tomato, cheese, ham", "Price DKK 70");
        System.out.printf("%-20.20s %-30.30s %-20.20s%n", "3.\tSalame: tomato", "cheese, pepperoni", "Price DKK 70");
        System.out.printf("%-20.20s %-30.30s %-20.20s%n", "4.\tFour formaggi:", "tomato, four kinds of cheese", "Price DKK 80");
        System.out.printf("%-20.20s %-30.30s %-20.20s%n", "5.\tVegetarian:", "chef's imagination", "Price DKK 75");
        System.out.printf("%-20.20s %-30.30s %-20.20s%n", "6.\tVegan:", "bread", "Price DKK 40");
        System.out.printf("%-20.20s %-30.30s %-20.20s%n", "7.\tHawaii:", "tomato, cheese, ham, pineapple", "Price DKK 60");
        System.out.printf("%-20.20s %-30.30s %-20.20s%n", "8.\tFrejas Favorites:", "tomato, cheese, ham, mushrooms", "Price DKK 70");
        System.out.printf("%-20.20s %-30.30s %-20.20s%n", "9.\tKebab:", "tomato, cheese, kebab, salad", "Price DKK 80");
        System.out.printf("%-21.21s %-30.30s %-20.20s%n", "10.\tMake your own:", "choose four toppings", "Price DKK 45");
    }
    private static void toppings() {
        System.out.printf("%-20.20s %-20.20s%n", "0.\t", "Go back");
        System.out.printf("%-20.20s %-20.20s%n", "1.\tCheese", "Price DKK 10");
        System.out.printf("%-20.20s %-20.20s%n", "2.\tTruffle", "Price DKK 20");
        System.out.printf("%-20.20s %-20.20s%n", "3.\tGarlic", "Price DKK 5");
        System.out.printf("%-20.20s %-20.20s%n", "4.\tMushrooms", "Price DKK 5");
        System.out.printf("%-20.20s %-20.20s%n", "5.\tPepper", "Price DKK 5");
        System.out.printf("%-20.20s %-20.20s%n", "6.\tOnion", "Price DKK 5");
        System.out.printf("%-20.20s %-20.20s%n", "7.\tHam", "Price DKK 8");
        System.out.printf("%-20.20s %-20.20s%n", "8.\tPepperoni", "Price DKK 8");
        System.out.printf("%-20.20s %-20.20s%n", "9.\tKebab", "Price DKK 8");
        System.out.printf("%-21.21s %-20.20s%n", "10.\tSalad", "Price DKK 3");

    }
    private static void sizes(double childSizePrice, double standardSizePrice, double familySizePrice) {
        System.out.println("Java Pizza offer 3 different sizes of your chosen Pizza: ");
        System.out.println("1.\tChild \t\t Price DKK " + childSizePrice);
        System.out.println("2.\tStandard \t Price DKK " + standardSizePrice);
        System.out.println("3.\tFamily \t\t Price DKK " + familySizePrice);
    }
}
